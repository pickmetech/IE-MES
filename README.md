基于JavaEE的开源制造执行系统，适用于教学或研究。


软件架构
Spring+Shiro+MyBatis+MySQL


安装教程
1. 创建MySQL数据库，执行数据库脚本，目录：src\main\webapp\dbscript\iemes_v1.sql
2. 源码采用maven管理，可以直接导入eclipse，更改jdbc.properties数据源配置即可run，登录账号：admin，密码：123456。